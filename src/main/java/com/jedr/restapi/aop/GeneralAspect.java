package com.jedr.restapi.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Date;

@Aspect
@Slf4j
@Component
public class GeneralAspect {
    @Pointcut("execution(* com.jedr.restapi.controller.*.*(..))")
    private void setLog(){}

    @Before("setLog()")
    public void before(JoinPoint joinPoint){
        log.info("Before method invoked::"+joinPoint.getSignature() + "Started at" + new Date());
    }

    @After("setLog()")
    public void after(JoinPoint joinPoint){
        log.info("After method invoked::"+joinPoint.getSignature());
    }

}
