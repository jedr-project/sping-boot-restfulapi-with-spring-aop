package com.jedr.restapi.controller;

import com.jedr.restapi.entity.RestApiEntity;
import com.jedr.restapi.service.RestApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RestApiController {

    @Autowired
    RestApiService apiService;

    @PostMapping("/addData")
    public String addData (@RequestBody RestApiEntity restApi){
        return   apiService.createData(restApi);
    }

    @GetMapping("/getData")
    public List<RestApiEntity> getdata(){
        return apiService.getAllData();
    }

    @PutMapping("/updateData")
    public String updateDate(@RequestBody RestApiEntity restApi ){
        return apiService.updateData(restApi);
    }

    @DeleteMapping("delete/{id}")
    public String deleteData(@PathVariable("id") Long id){
        return apiService.deleteData(id);
    }

}
