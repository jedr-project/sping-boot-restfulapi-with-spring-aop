# Sping boot RestfulApi with Spring AOP


## Description
Spring AOP is a feature in the Spring Framework that helps developers separate common tasks, like logging or security, from the main code of an application. It allows you to define these tasks in separate modules called aspects and apply them to different parts of your code without mixing them with your main business logic. This makes your code cleaner, easier to maintain, and more organized.

![Alt text](<Pasted Graphic.png>)


